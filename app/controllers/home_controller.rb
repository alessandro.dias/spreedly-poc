class HomeController < ApplicationController
  def index
  end

  def purchase

    payment_method_token = add_credit_card(params[:purchase][:number])
    response = purchase_request(params[:purchase][:amount], payment_method_token)

    render json: JSON.parse(response.body)
  end

  def handle_callback
    render json: params
  end

  def handle_redirect
    render json: params
  end

  private

  def add_credit_card(number)
    response = conn.post('/v1/payment_methods.json') do |req|
      req.body = {
        payment_method: {
          credit_card: {
            first_name: "Joe",
            last_name: "Jones",
            number: number,
            verification_value: "123",
            month: "10",
            year: "2029",
            company: "Acme Inc.",
            address1: "33 Lane Road",
            address2: "Apartment 4",
            city: "Wanaque",
            state: "NJ",
            zip: "31331",
            country: "US",
            phone_number: "919.331.3313",
            shipping_address1: "33 Lane Road",
            shipping_address2: "Apartment 4",
            shipping_city: "Wanaque",
            shipping_state: "NJ",
            shipping_zip: "31331",
            shipping_country: "US",
            shipping_phone_number: "919.331.3313"
          },
          email: "joey@example.com",
          metadata: {}
        }
      }.to_json
    end

    JSON.parse(response.body)["transaction"]["payment_method"]["token"]
  end


  def purchase_request(amount, payment_method)
    conn.post("/v1/gateways/#{ENV.fetch("GATEWAY")}/purchase.json") do |req|
      req.body = {
        transaction: {
          payment_method_token: payment_method,
          amount: 100,
          currency_code: "USD",
          attempt_3dsecure: true,
          callback_url: handle_callback_url(host: ENV["HOST"], protocol: :http, port: 80),
          redirect_url: handle_redirect_url(host: ENV["HOST"], protocol: :http, port: 80),
          three_ds_version: "2",
          browser_info: {
            browser_size: '01',
            acceptHeader: 'text/html'
          }
        }
      }.to_json
    end
  end

  def conn
    @conn ||= Faraday.new(
      url: "https://core.spreedly.com",
      headers: {'Content-Type' => 'application/json'}
    ) do |conn|
      conn.basic_auth(ENV.fetch("ENV_KEY"), ENV.fetch("ACCESS_SECRET"))
    end
  end
end
